<?php
include 'database.php';
class Forward_chaining
{
    function __construct()
    {
        $this->db = new Database();
    }

    function generate($data_input)
    {
        $knowledge = $this->generate_knowledge();
        foreach ($data_input as $key => $value) {
            $knowledge[$key] = $value;
        }

        $indikator = null;

        $hasil = "Bakat Belum Teridentifikasi";
        if ($knowledge['C1'] && $knowledge['C2'] && $knowledge['C3']) {
            $knowledge['I1'] = '1';
            $indikator['I1'] = 'Tingkat perbendaharaan kata yang tinggi';
        }
        if ($knowledge['C4'] && $knowledge['C5']) {
            $knowledge['I2'] = '1';
            $indikator['I2'] = 'Mempunyai Ingatan kuat';
        }
        if ($knowledge['C6'] && $knowledge['C7']) {
            $knowledge['I2'] = '1';
            $indikator['I2'] = 'Mempunyai Ingatan kuat';
        }
        if ($knowledge['C8'] && $knowledge['C9']) {
            $knowledge['I2'] = '1';
            $indikator['I2'] = 'Mempunyai Ingatan kuat';
        }
        if ($knowledge['C10'] && $knowledge['C11'] && $knowledge['C12'] && $knowledge['C13']) {
            $knowledge['I3'] = '1';
            $indikator['I3'] = 'Penguasaan kata - kata abstrak';
        }
        if ($knowledge['C14'] && $knowledge['C15'] && $knowledge['C16']) {
            $knowledge['I4'] = '1';
            $indikator['I4'] = 'Memiliki Pemikiran abstrak';
        }
        if ($knowledge['C17'] && $knowledge['C18']) {
            $knowledge['I5'] = '1';
            $indikator['I5'] = 'Memiliki Prestasi bidang matematika';
        }
        if ($knowledge['C19'] && $knowledge['C20']) {
            $knowledge['I5'] = '1';
            $indikator['I5'] = 'Memiliki Prestasi bidang matematika';
        }
        if ($knowledge['C21'] && $knowledge['C22'] && $knowledge['C23']) {
            $knowledge['I5'] = '1';
            $indikator['I5'] = 'Memiliki Prestasi bidang matematika';
        }
        if ($knowledge['I1'] && $knowledge['I2']) {
            $knowledge['K1'] = '1';
        }
        if ($knowledge['I1'] && $knowledge['I3']) {
            $knowledge['K1'] = '1';
        }
        if ($knowledge['I2'] && $knowledge['I3']) {
            $knowledge['K1'] = '1';
            $hasil = "Intelektual Umum";
        }
        if ($knowledge['I4'] && $knowledge['I5']) {
            $knowledge['K2'] = '1';
            $hasil = "Akademik Khusus";
        }
        if ($knowledge['K1'] && $knowledge['K2']) {
            $knowledge['K3'] = '1';
            $hasil = "Multitalenta";
        }

        $output = [
            'hasil' => $hasil,
            'indikator' => $indikator
        ];

        return $output;
    }

    private function generate_knowledge()
    {
        $data_variabel = $this->db->ambil_data();
        foreach ($data_variabel as $item) {
            $knowledge[$item['kode']] = '0';
        }
        return $knowledge;
    }
}
