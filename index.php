<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Sistem Pakar Bakat Anak</title>
</head>

<body class="container px-5 pb-5 pt-4">
    <header>
        <h3 style="margin: 20px;" class="text-center">Sistem Pakar Bakat Anak</h3>
        <?php
        include 'forward_chaining.php';
        $db = new Database();
        if (isset($_POST['submit'])) {
            $i = 0;
            foreach ($_POST as $key => $value) {
                $array[$key] = $value;
                if ($value) {
                    $set_checked[$i]['ya'] = 'checked';
                    $set_checked[$i]['tidak'] = '';
                } else {
                    $set_checked[$i]['ya'] = '';
                    $set_checked[$i]['tidak'] = 'checked';
                }

                $i++;
            }

            $fc = new Forward_chaining();

            $hasil = $fc->generate($array);

        ?>
            <div class="card mb-3 bg-light">
                <div class="card-header"> Hasil Analisis </div>
                <div class="card-body">
                    <h4 class="text-dark my-2">Memiliki Bakat : <span class="text-danger"><?= $hasil['hasil'] ?></span></h4>
                    <p class="text-dark mb-1">Indikator Terpenuhi :</p>
                    <ul>
                        <?php
                        if ($hasil['indikator'] != null) {
                            foreach ($hasil['indikator'] as $value) { ?>
                                <li><?= $value; ?></li>
                        <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        <?php } else {
            for ($i = 0; $i < 24; $i++) {
                $set_checked[$i]['ya'] = '';
                $set_checked[$i]['tidak'] = '';
            }
        }
        ?>
    </header>
    <main style="margin-top: 20px;">

        <div>
            <form action="" method="post">
                <?php
                $c = $db->ambil_data('c');
                $i = 0;
                foreach ($c as $data) :
                ?>
                    <div class="card mb-3 bg-light">
                        <div class="card-body">
                            <label for="" class="card-title"><?= $data['keterangan'] ?>?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="<?= $data['kode'] ?>" value="1" id="<?= $data['kode'] ?>ya" required <?= $set_checked[$i]['ya'] ?>>
                                <label class="form-check-label" for="<?= $data['kode'] ?>ya">
                                    Ya
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="<?= $data['kode'] ?>" value="0" id="<?= $data['kode'] ?>tidak" required <?= $set_checked[$i]['tidak'] ?>>
                                <label class="form-check-label" for="<?= $data['kode'] ?>tidak">
                                    Tidak
                                </label>
                            </div>
                        </div>
                    </div>
                <?php $i++;
                endforeach; ?>
                <button class="btn btn-primary" name="submit">Submit</button>
            </form>

        </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>

</html>