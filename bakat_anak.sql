-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2021 at 10:45 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bakat_anak`
--

-- --------------------------------------------------------

--
-- Table structure for table `bakat`
--

DROP TABLE IF EXISTS `bakat`;
CREATE TABLE `bakat` (
  `id` int(11) NOT NULL,
  `kode` varchar(4) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bakat`
--

INSERT INTO `bakat` (`id`, `kode`, `keterangan`, `jenis`) VALUES
(1, 'C1', 'Dapat menirukan kalimat sederhana', 'c'),
(2, 'C2', 'Dapat meniru kembali 4-5 urutan kata', 'c'),
(3, 'C3', 'Mengulangi kalimat yang sudah didengarnya', 'c'),
(4, 'C4', 'Menyanyikan lagu anak-anak lebih dari 20 lebih lagu', 'c'),
(5, 'C5', 'Mengucapkan syair lagu sambil diiringi senandung lagunya', 'c'),
(6, 'C6', 'Dapat menyebutkan simbol-simbol huruf vokal dan konsonan', 'c'),
(7, 'C7', 'Meniru berbagai lambang huruf vokal dan konsonan', 'c'),
(8, 'C8', 'Dapat mengelompokkan benda dengan berbagai cara menurut fungsinya', 'c'),
(9, 'C9', 'Mengelompokkan benda dengan berbagai cara menurut fungsinya : misalnya peralatan makan, peralatan mandi, peralatan kebersihan', 'c'),
(10, 'C10', 'Bercerita tentang gambar yang disediakan atau dibuat sendiri', 'c'),
(11, 'C11', 'Bercerita menggunakan kata ganti aku, saya, kamu, mereka, dlL', 'c'),
(12, 'C12', 'Dapat Menggunakan dan dapat menjawab pertanyaan apa, mengapa, dimana, berapa, bagaimana, dsb', 'c'),
(13, 'C13', 'Memberikan keterangan/informasi tentang suatu haL', 'c'),
(14, 'C14', 'Dapat menyebutkan urutan bilangan 1-10', 'c'),
(15, 'C15', 'Dapat menunjuk lambang bilangan 1-10', 'c'),
(16, 'C16', 'Meniru lambang bilangan 1-10', 'c'),
(17, 'C17', 'Membedakan dan membuat dua kumpulan benda berdasarkan kuantitasnya.', 'c'),
(18, 'C18', 'Mengenal perbedaan benda berdasarkan bentuknya', 'c'),
(19, 'C19', 'Mencoba dan menceritakan tentang proses pencampuran warna.', 'c'),
(20, 'C20', 'Mencoba dan menceritakan tentang proses benda-benda dimasukkan kedalam air (terapung, melayang, tenggelam)', 'c'),
(21, 'C21', 'Menceritakan macam-macam bunyi', 'c'),
(22, 'C22', 'Menceritakan macam-macam rasa', 'c'),
(23, 'C23', 'Menceritakan macam-macam bau', 'c'),
(24, 'I1', 'Tingkat perbendaharaan kata yang tinggi', 'i'),
(25, 'I2', 'Mempunyai Ingatan kuat', 'i'),
(26, 'I3', 'Penguasaan kata - kata abstrak', 'i'),
(27, 'I4', 'Memiliki Pemikiran abstrak', 'i'),
(28, 'I5', 'Memiliki Prestasi bidang matematika', 'i'),
(29, 'K1', 'Intelektual Umum', 'k'),
(30, 'K2', 'Akademik Khusus', 'k'),
(31, 'K3', 'Multitalenta', 'k');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bakat`
--
ALTER TABLE `bakat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bakat`
--
ALTER TABLE `bakat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
